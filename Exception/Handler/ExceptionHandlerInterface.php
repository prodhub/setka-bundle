<?php

namespace ADW\SetkaBundle\Exception\Handler;

use GuzzleHttp\Exception\RequestException;
use ADW\RestClientBundle\Description\MethodDescriptionInterface;

/**
 * Interface ExceptionHandlerInterface.
 * Project sberbank-blog.
 * @author Anton Prokhorov
 */
interface ExceptionHandlerInterface
{
    public function handle(RequestException $exception, MethodDescriptionInterface $methodDescriptionInterface, array $options);
}
