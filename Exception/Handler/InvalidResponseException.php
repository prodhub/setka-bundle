<?php

namespace ADW\SetkaBundle\Exception;

/**
 * Class InvalidResponseException.
 * Project sberbank-blog.
 * @author Anton Prokhorov
 */
class InvalidResponseException extends \RuntimeException
{
}
