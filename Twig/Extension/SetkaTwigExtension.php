<?php

namespace ADW\SetkaBundle\Twig\Extension;

use ADW\SetkaBundle\Entity\SetkaImage;
use ADW\SetkaBundle\Manager\SetkaManager;
use Doctrine\ORM\EntityManager;
use Twig_Error_Runtime;

/**
 * Class SetkaTwigExtension.
 * Project sberbank-blog.
 * @author Anton Prokhorov
 */
class SetkaTwigExtension extends \Twig_Extension
{

    /**
     * @var SetkaManager
     */
    protected $setkaManager;


    /*
     * @var EntityManager $em
     */
    private $em;

    /**
     * SetkaTwigExtension constructor.
     * @param SetkaManager $setkaManager
     */
    public function __construct(EntityManager $em, SetkaManager $setkaManager)
    {
        $this->em = $em;
        $this->setkaManager = $setkaManager;
    }


    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('setka_get_editor', function () {
                try {
                    $result = $this->setkaManager->getCurrentBuild();
                } catch (\Exception $exception) {
                    $result = false;
                }

                return $result;
            }),
            new \Twig_SimpleFunction('setka_get_editor_js', function () {
                $result = $this->setkaManager->getCurrentBuild();
                return $result['content_editor_files'][1]['url'];
            }),
            new \Twig_SimpleFunction('setka_get_editor_theme_css', function () {
                $result = $this->setkaManager->getCurrentBuild();
                return $result['theme_files'][0]['url'];
            }),
            new \Twig_SimpleFunction('setka_get_editor_css', function () {
                $result = $this->setkaManager->getCurrentBuild();
                return $result['content_editor_files'][0]['url'];
            }),
            new \Twig_SimpleFunction('setka_get_company_json', function () {
                $result = $this->setkaManager->getCurrentBuild();
                return $result['theme_files'][1]['url'];
            }),
            new \Twig_SimpleFunction('setka_get_public_token', function () {
                $result = $this->setkaManager->getCurrentBuild();
                return $result['public_token'];
            }),
            new \Twig_SimpleFunction('postImages', [$this, 'postImages'])
        ];
    }


    /**
     * @param array $ids
     * @return SetkaImage[]|array|bool
     */
    public function postImages(array $ids)
    {
        $imagePosts = $this->em->getRepository(SetkaImage::class)->findBy(['id' => $ids], ['id' => 'DESC']);

        if ($imagePosts) {
            return $imagePosts;
        }

        return false;
    }
}
