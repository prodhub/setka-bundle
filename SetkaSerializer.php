<?php
/**
 * Class SetkaSerializer.
 * Project sberbank-blog.
 * @author Anton Prokhorov
 */

namespace ADW\SetkaBundle;

use ADW\SetkaBundle\Exception\InvalidResponseException;
use JMS\Serializer\Context;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\Serializer;

class SetkaSerializer
{

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * AiloveSerializer constructor.
     * @param Serializer $serializer
     */
    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param $json
     * @return array
     */
    public function decode($json)
    {
        $decoded = json_decode($json, true);

        if (!array_key_exists('data', $decoded)) {
            throw new InvalidResponseException('Invalid CRM response: missing data field. It\'s PAYAL\'NIK time!');
        }

        return $decoded['data'];
//        return $decoded;
    }

    /**
     * @param array $data
     * @param $type
     * @param DeserializationContext|null $context
     * @return mixed
     */
    public function normalize(array $data, $type, DeserializationContext $context = null)
    {
        return $this->serializer->fromArray($data, $type, $context);
    }

    /**
     * @param Context $source
     * @param Context $target
     * @return Context
     */
    public function fillContext(Context $source, Context $target)
    {
        if ($exclusionStrategy = $source->getExclusionStrategy()) {
            $target->addExclusionStrategy($exclusionStrategy);
        }

        $target->attributes = $source->attributes;

        return $target;
    }
}
