<?php


namespace ADW\SetkaBundle\RestClient;

/**
 * interface CustomResponseHandlerInterface.
 * Project sberbank-blog.
 * @author Anton Prokhorov
 */

interface CustomResponseHandlerInterface
{
    public function getCustomHandler();
}
