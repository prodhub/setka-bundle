<?php
/**
 * Class SaveSnippetMethodDescription.
 * Project sberbank-blog.
 * @author Anton Prokhorov
 */

namespace ADW\SetkaBundle\RestClient\Method\Setka;

use ADW\SetkaBundle\RestClient\Method\AbstractSetkaMethodDescription;

class SaveSnippetMethodDescription extends AbstractSetkaMethodDescription
{

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'array';
    }

    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'token' => 'string',
            'theme_id' => 'int',
            'snippet' => ['array', ['name' => 'string', 'code' => 'string']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/v1/custom/snippets';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return $options;
    }
}
