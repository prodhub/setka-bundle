<?php

namespace ADW\SetkaBundle\RestClient\Method\Setka;

use ADW\SetkaBundle\RestClient\Method\AbstractSetkaMethodDescription;

/**
 * Class GetCustomBuildsCurrentMethodDescription.
 * Project sberbank-blog.
 * @author Anton Prokhorov
 */
class GetCustomBuildsCurrentMethodDescription extends AbstractSetkaMethodDescription
{

    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'token' => 'string'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/v1/custom/builds/current';
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'GET';
    }


    /**
     * {@inheritdoc}
     */
    public function getQuery(array $options)
    {
        return $options;
    }


    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'array';
    }
}
