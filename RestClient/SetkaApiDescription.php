<?php

namespace ADW\SetkaBundle\RestClient;

use ADW\SetkaBundle\Exception\Handler\ExceptionHandlerInterface;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Symfony\Component\HttpFoundation\RequestStack;
use ADW\RestClientBundle\Client\ClientDescriptionInterface;
use ADW\RestClientBundle\Event\ExceptionEvent;
use ADW\RestClientBundle\Event\RequestEvent;

/**
 * Class SetkaApiDescription.
 * Project sberbank-blog.
 * @author Anton Prokhorov
 */
class SetkaApiDescription implements ClientDescriptionInterface
{
    const API_BASE_HOST_SCHEMA = 'https';

    const API_REQ_URL = 'editor.setka.io';


    /** @var Serializer */
    protected $serializer;

    /** @var RequestStack */
    protected $requestStack;


    /**
     * @var ExceptionHandlerInterface[]
     */
    protected $exceptionHandlers = [];


    /**
     * SetkaApiDescription constructor.
     * @param Serializer $serializer
     * @param RequestStack $requestStack
     * @param EntityManager $em
     */
    public function __construct(Serializer $serializer, RequestStack $requestStack)
    {
        $this->serializer = $serializer;
        $this->requestStack = $requestStack;
        $this->host = self::API_REQ_URL;
    }


    /**
     * @param ExceptionHandlerInterface $exceptionHandler
     */
    public function addExceptionHandler(ExceptionHandlerInterface $exceptionHandler)
    {
        $this->exceptionHandlers[] = $exceptionHandler;
    }


    /**
     * @return string
     */
    public function getHost()
    {
        return self::API_REQ_URL;
    }

    /**
     * @return string
     */
    public function getSchema()
    {
        return self::API_BASE_HOST_SCHEMA;
    }


    /**
     * @return callable
     */
    public function getSerializer()
    {
        return function ($data, $format, array $context) {
            $jmsContext = SerializationContext::create();

            if (array_key_exists('groups', $context)) {
                $jmsContext->setGroups($context['groups']);
            }

            foreach ($context as $name => $value) {
                $jmsContext->setAttribute($name, $value);
            }

            $jmsContext->setAttribute('setka', true);

            return $this->serializer->serialize($data, 'setka_json', $jmsContext);
        };
    }

    /**
     * @return callable
     */
    public function getDeserializer()
    {
        return function ($description, $data, $format, $model, array $context) {
            if ($description instanceof CustomResponseHandlerInterface) {
                $handler = $description->getCustomHandler();

                return $handler($this->serializer, $description, $data, $format, $model, $context);
            }

            $jmsContext = DeserializationContext::create();

            if (array_key_exists('groups', $context)) {
                $jmsContext->setGroups($context['groups']);
            }

            foreach ($context as $name => $value) {
                $jmsContext->setAttribute($name, $value);
            }

            $jmsContext->setAttribute('_context', clone $jmsContext);

            return $this->serializer->fromArray(json_decode($data, true), $model, $jmsContext);
        };
    }


    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            [
                'event' => RequestEvent::NAME,
                'listener' => [$this, 'setHeaders'],
            ],
            [
                'event' => ExceptionEvent::NAME,
                'listener' => function (ExceptionEvent $event) {
                    $exception = $event->getException();

                    foreach ($this->exceptionHandlers as $handler) {
                        $handler->handle($exception, $event->getMethodDescription(), $event->getOptions());
                    }

                    throw $exception;
                },
            ],
        ];
    }


    /**
     * @param RequestEvent $event
     */
    public function setHeaders(RequestEvent $event)
    {
        $request = $event->getRequest();

        $request = $request
            ->withHeader('Accept-Language', 'ru');



        $event->setRequest($request);
    }

    /**
     * @param RequestEvent $event
     */
    public function prepareRequest(RequestEvent $event)
    {
        $request = $event->getRequest();

        $oldOptions = $event->getOptions();

        $newOptions = [
//            'access_token' => $this->access['service_access_key']
        ];

        $paramsOptions = array_merge($newOptions, $oldOptions);

        $modify = [];

        $modify['query'] = http_build_query($paramsOptions);

        $event->setRequest(\GuzzleHttp\Psr7\modify_request($request, $modify));
    }
}
