<?php


namespace ADW\SetkaBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use ADW\GuzzleBundle\DependencyInjection\GuzzleDependencyInjectionTrait;

/**
 * Class ADWSetkaExtension.
 * Project sberbank-blog.
 * @author Anton Prokhorov
 */
class ADWSetkaExtension extends Extension
{
    use GuzzleDependencyInjectionTrait;

    const GUZZLE_ID = 'adw_setka.guzzle';
    const CLIENT_ID = 'adw_setka_client';


    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        //set config
        foreach ($config as $key => $value) {
            $container->setParameter('adwsetka.config.' . $key, $value);
        }

        $config['guzzle_options'] = [
            'headers' => [
                'User-Agent' => 'ADW'
            ]
        ];

        $stack = [];
        $logger = $this->buildLogger(
            $container,
            'adwsetka',
            'adwsetka',
            '>>> {request} <<< {response}'
        );

        $guzzle = $this->buildClient(
            $container,
            'adwsetka',
            null,
            $stack,
            $config['guzzle_options'],
            true,
            $logger
        );
        $container->setAlias(self::GUZZLE_ID, $guzzle);

        $container->setDefinition(
            self::CLIENT_ID,
            new Definition(
                'ADW\RestClientBundle\Client\Client',
                [
                    new Reference(self::GUZZLE_ID),
                    new Reference('adw_setka.description')
                ]
            )
        );

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
//        $loader->load('repository.yml');
        $loader->load('serializer.yml');
    }
}
