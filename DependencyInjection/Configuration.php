<?php


namespace ADW\SetkaBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration.
 * Project sberbank-blog.
 * @author Anton Prokhorov
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('adw_setka');
        $rootNode
            ->children()
                ->scalarNode('license_key')->isRequired()->cannotBeEmpty()->end()
            ->end();
        return $treeBuilder;
    }
}
