<?php


namespace ADW\SetkaBundle\Serializer;

use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\VisitorInterface;

/**
 * Class SetkaDataHandler.
 * Project sberbank-blog.
 * @author Anton Prokhorov
 */
class SetkaDataHandler implements SubscribingHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribingMethods()
    {
        $methods = [];
        $formats = ['json', 'setka_json'];
        $collectionTypes = [
            'SetkaData',
        ];

        foreach ($collectionTypes as $type) {
            foreach ($formats as $format) {
                $methods[] = [
                    'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                    'type' => $type,
                    'format' => $format,
                    'method' => 'deserialize',
                ];
            }
        }

        return $methods;
    }

    /**
     * @param VisitorInterface $visitor
     * @param $data
     * @param array   $type
     * @param Context $context
     *
     * @return mixed
     */
    public function deserialize(VisitorInterface $visitor, $data, array $type, Context $context)
    {
        $type = $context->attributes->get('_setka_type')->get();

        return $context->accept($data, $type);
    }
}
