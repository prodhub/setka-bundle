<?php

namespace ADW\SetkaBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ADWSetkaBundle.
 * Project sberbank-blog.
 * @author Anton Prokhorov
 */
class ADWSetkaBundle extends Bundle
{
}
