<?php

namespace ADW\SetkaBundle\Form\Type;

use ADW\SetkaBundle\Entity\SetkaImage;
use Sonata\MediaBundle\Model\Media;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sonata\CoreBundle\Validator\ErrorElement;

/**
 * Class ImagesFormType.
 * Project sberbank-blog.
 * @author Anton Prokhorov
 */
class ImagesFormType extends AbstractType
{
    const ERROR_MESSAGE = 'Некорректный формат файла';
    const ALLOWED_TYPES = [
        'image/gif',
        'image/bmp',
        'image/svg+xml',
        'image/jpeg',
        'image/pjpeg',
        'image/png'
    ];

    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'file',
                FileType::class,
                [
                    'label' => 'Картинка',
                ]
            )

        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false
        ]);
    }

    public function validateBlock(ErrorElement $errorElement, SetkaImage $image)
    {
        if (!in_array($image->getImage()->getContentType(), self::ALLOWED_TYPES)) {
            $errorElement->addViolation(self::ERROR_MESSAGE);
        }
    }
}
