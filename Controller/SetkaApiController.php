<?php

namespace ADW\SetkaBundle\Controller;

use ADW\CommonBundle\Annotation\ApiDoc;
use ADW\CommonBundle\Controller\Controller;
use ADW\CommonBundle\Exception\InvalidFormException;
use Doctrine\ORM\EntityNotFoundException;
use Ivory\CKEditorBundle\Exception\Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Delete;
use ADW\SetkaBundle\Entity\SetkaImage;
use ADW\SetkaBundle\Form\Type\ImagesFormType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Application\Sonata\MediaBundle\Entity\Media;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class SetkaApiController.
 * Project sberbank-blog.
 * @author Anton Prokhorov
 * @Route("/api/setka-editor/v1")
 */
class SetkaApiController extends Controller
{
    /**
     * Api Main
     *
     * @Route("/", name="setka_api_main", options={"expose"=true})
     */
    public function indexAction()
    {
        return [];
    }

    /**
     * Загрузка картинки
     *
     * @ApiDoc(
     *      section="Setka Api",
     *      parameters={
     *          {"name"="file", "dataType"="file", "description"="Картинка", "required"=true}
     *      }
     * )
     * @Route("/images", name="api.setka.images", options={"expose"=true})
     * @Method("POST")
     * @View()
     */
    public function imagesAction(Request $request)
    {
        $form = $this->createForm(new ImagesFormType());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            /** @var UploadedFile $file */
            $file = $data['file'];

            $media = new Media();
            $media->setName($file->getClientOriginalName());
            $media->setBinaryContent($file);
            $media->setEnabled(true);
            $media->setContext('default');
            $media->setProviderName('sonata.media.provider.image');

            $em = $this->getDoctrine()->getManager();

            $image = new SetkaImage(
                $media
            );

            $image->setAlt($file->getClientOriginalName());

            $em->persist($image);
            $em->flush();

            $provider = $this->container->get($media->getProviderName());
            $format = $provider->getFormatName($media, 'small');

            $urlReference = $this->container->get('request')->getSchemeAndHttpHost() . $provider->generatePublicUrl($image->getImage(), 'reference');
            $urlSmall = $this->container->get('request')->getSchemeAndHttpHost() . $provider->generatePublicUrl($image->getImage(), $format);

            $data = [
                'id' => $image->getId(),
                'url' => $urlReference,
                'thumbUrl' => $urlSmall
            ];

            return $data;
        } else {
            throw new InvalidFormException($form);
        }
    }


    /**
     * Загрузка сниппета
     *
     * @ApiDoc(
     *      section="Setka Api",
     *      parameters={
     *          {"name"="code", "dataType"="string", "description"="Html Код", "required"=true},
     *          {"name"="id", "dataType"="string", "description"="Id сниппета", "required"=true},
     *          {"name"="name", "dataType"="string", "description"="Имя сниппета", "required"=true},
     *          {"name"="themeId", "dataType"="string", "description"="Id темы", "required"=true},
     *      }
     * )
     * @Route("/snippets", name="api.setka.snippets", options={"expose"=true})
     * @Method("POST")
     * @View()
     */
    public function snippetsAction(Request $request)
    {
        $result = $this->get('adw_setka.manager')->saveSnippet($request->get('themeId'), ['name' => $request->get('name'), 'code' => $request->get('code') ]);

        return $result;
    }


    /**
     * Получить инфу по важной статье
     *
     * @ApiDoc(
     *      section="Setka Api"
     * )
     * @Route("/impotant", name="api.setka.impotant", options={"expose"=true})
     * @Method("POST")
     * @View()
     */
    public function getImpotantAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $important = $em->getRepository('AppBundle:Post')->findImportantPost();

        $provider = $this->container->get($important->getBannerImage()->getProviderName());
        $format = $provider->getFormatName($important->getBannerImage(), 'small');

        $urlSmall = $this->container->get('request')->getSchemeAndHttpHost() . $provider->generatePublicUrl($important->getBannerImage(), $format);

        return [
            'image' => $urlSmall,
            'title' => $important->getSummary(),
            'link' => $this->container->get('request')->getSchemeAndHttpHost() . $this->generateUrl('setka_item', ['slug' => $important->getSlug()])
        ];
    }

    /**
     * Получить картинки
     *
     * @ApiDoc(
     *      section="Setka Api",
     *      parameters={
     *          {"name"="ids", "dataType"="string", "required"=false, "description"="IDs Картинок"}
     *      }
     * )
     * @Route("/images/get", name="api.setka.images.get", options={"expose"=true})
     * @Method("POST")
     * @View()
     */
    public function getImageAction(Request $request)
    {
        $idsPics  = explode(',', $request->get('ids'));

        $em = $this->getDoctrine()->getManager();

        $imagePosts = $em->getRepository(SetkaImage::class)->findBy(['id' => $idsPics], ['id' => 'DESC']);

        $data  = [];

        if (!$imagePosts) {
            return new JsonResponse($data);
        }

        foreach ($imagePosts as $imagePost) {
            $provider = $this->container->get($imagePost->getImage()->getProviderName());
            $format = $provider->getFormatName($imagePost->getImage(), 'small');

            $urlReference = $this->container->get('request')->getSchemeAndHttpHost() . $provider->generatePublicUrl($imagePost->getImage(), 'reference');
            $urlSmall = $this->container->get('request')->getSchemeAndHttpHost() . $provider->generatePublicUrl($imagePost->getImage(), $format);


            $data[] = [
                'id' => $imagePost->getId(),
                'name' => $imagePost->getImage()->getName(),
                'url' => $urlReference,
                'thumbUrl' => $urlSmall,
                'alt' => $imagePost->getAlt()
            ];
        }

        return new JsonResponse($data);
    }

    /**
     * Удаление картинки
     *
     * @ApiDoc(
     *      section="Setka Api",
     *      parameters={
     *          {"name"="id", "dataType"="string", "required"=false, "description"="ID Картинки"}
     *      }
     * )
     * @Route("/images/{id}", name="api.setka.images.remove", options={"expose"=true})
     * @Method("DELETE")
     *
     */
    public function deleteImageAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $imagePost = $em->getRepository(SetkaImage::class)->findOneBy(['id' => $id]);

        if (!$imagePost) {
            throw $this->createNotFoundException('The Image does not exist');
        }

        $em->remove($imagePost);
        $em->flush();

        $response = new JsonResponse(['delete' => 'success']);

        return $response;
    }


    /**
     * Изменение картинки
     *
     * @ApiDoc(
     *      section="Setka Api",
     *      parameters={
     *          {"name"="id", "dataType"="string", "required"=false, "description"="ID Картинки"}
     *      }
     * )
     * @Route("/images/{id}", name="api.setka.images.put", options={"expose"=true})
     * @Method("PUT")
     *
     */
    public function putImageAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        /**
         * @var SetkaImage $imagePost
         */
        $imagePost = $em->getRepository(SetkaImage::class)->findOneBy(['id' => $id]);

        if (!$imagePost) {
            throw $this->createNotFoundException('The Image does not exist');
        }

        $imagePost->setAlt($request->get('alt'));

        $em->persist($imagePost);
        $em->flush();

        $response = new JsonResponse(['update' => 'success']);

        return $response;
    }
}
