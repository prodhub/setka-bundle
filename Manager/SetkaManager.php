<?php

namespace ADW\SetkaBundle\Manager;

use ADW\SetkaBundle\RestClient\Method\Setka\GetCustomBuildsCurrentMethodDescription;
use ADW\SetkaBundle\RestClient\Method\Setka\SaveSnippetMethodDescription;
use Symfony\Bridge\Monolog\Logger;
use ADW\RestClientBundle\Client\Client;

/**
 * Class SetkaManager.
 * Project sberbank-blog.
 * @author Anton Prokhorov
 */
class SetkaManager
{

    /*
    * @var Client
    */
    protected $client;

    /**
     * @var Logger
     */
    protected $logger;


    protected $license;

    /**
     * SetkaManager constructor.
     * @param Client $client
     * @param Logger $logger
     * @param $license
     */
    public function __construct(Client $client, Logger $logger, $license)
    {
        $this->client = $client;
        $this->logger = $logger;
        $this->license = $license;
    }


    /**
     * @return mixed
     */
    public function getCurrentBuild()
    {
        $result = $this->client->request(new GetCustomBuildsCurrentMethodDescription(), [
            'token' => $this->license
        ]);

        return $result;
    }

    /**
     * @return mixed
     */
    public function saveSnippet($themeId, array $array)
    {
        $result = $this->client->request(new SaveSnippetMethodDescription(), [
            'token' => $this->license,
            'theme_id' => (int)$themeId,
            'snippet' => $array
        ]);

        return $result;
    }
}
