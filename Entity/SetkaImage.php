<?php

namespace ADW\SetkaBundle\Entity;

use Application\Sonata\MediaBundle\Entity\Media;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class SetkaImage
 *
 * @package ADW\SetkaImage\Entity
 * @author Anton Prokhorov
 *
 * @ORM\Entity()
 * @ORM\Table(name="app_setka_images")
 */
class SetkaImage
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var Media
     *
     * @ORM\OneToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $image;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $alt;

    /**
     * Image constructor.
     * @param Media $image
     */
    public function __construct(Media $image)
    {
        $this->image = $image;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return Media
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param Media $image
     * @return SetkaImage
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return string
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * @param string $alt
     * @return SetkaImage
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;
        return $this;
    }
}
